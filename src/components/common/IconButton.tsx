import { Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { theme } from '../../config/config';

type Props = {
    text?: string;
    logo?: any;
    type?: string;
    action?: () => void;
};

const IconButton = ({ text, logo, type, action }: Props) => {
    const styles = StyleSheet.create({
        container: {
            borderWidth: 1,
            flexDirection: 'row',
            alignItems: 'center',
            paddingVertical: 10,
            paddingHorizontal: 10,
            marginVertical: 10,
            width: 220,
            backgroundColor: type === 'default' ? theme.primaryMidGray : theme.primaryLight,
            borderRadius: 5,
            justifyContent: 'space-around'
        },
        text: {
            color: type === 'default' ? theme.primaryLight : theme.primaryDark,
            fontSize: type === 'default' ? 10 : 16
        },
        logo: {
            height: 20,
            width: 20,
            marginLeft: 10,
        },
    });

    return (
        <TouchableOpacity style={styles.container} onPress={action}>
            <Text style={styles.text}>{text}</Text>

            {logo && <Image source={logo} style={styles.logo} />}
        </TouchableOpacity>
    );
};



export default IconButton;
