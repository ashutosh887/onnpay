import { Text } from "react-native";
import React from "react";
import { commonStyles } from "../../config/config";

type Props = {
  fontSize?: number;
};

const Brand = ({ fontSize }: Props) => {
  return (
    <Text
      style={{
        fontSize: fontSize || 14,
        fontWeight: "700",
      }}
    >
      <Text style={commonStyles.yellowText}>O</Text>
      <Text style={commonStyles.lightText}>nn</Text>
      <Text style={commonStyles.yellowText}>P</Text>
      <Text style={commonStyles.lightText}>ay</Text>
    </Text>
  );
};

export default Brand;
