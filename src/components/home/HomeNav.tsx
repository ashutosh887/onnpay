import { View, Text, StyleSheet } from "react-native";
import React from "react";
import { balance, theme, username } from "../../config/config";
import { Button, IconButton } from "react-native-paper";

const HomeNav = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.navigationContainer}>
        <Button
          icon="account-circle-outline"
          mode="text"
          labelStyle={styles.buttonLabel}
          onPress={() => navigation.navigate("Profile")}
        >
          {username}
        </Button>

        <IconButton
          icon="cog"
          iconColor={theme.primaryLight}
          size={18}
          onPress={() => navigation.navigate("Settings")}
        />
      </View>

      <View style={styles.balanceContainer}>
        <Text
          style={{
            textAlign: "center",
            color: theme.primaryLight,
            fontSize: 16,
          }}
        >
          Balance
        </Text>
        <Text style={styles.balance}>{balance}</Text>
      </View>

      <View style={styles.actionContainer}>
        <Button
          icon="arrow-top-right"
          contentStyle={{ flexDirection: "row-reverse" }}
          labelStyle={styles.actionLabel}
          style={styles.action}
        >
          Send
        </Button>

        <Button
          icon="arrow-top-right"
          contentStyle={{ flexDirection: "row-reverse" }}
          labelStyle={styles.actionLabel}
          style={styles.action}
        >
          Recieve
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    backgroundColor: theme.primaryDark,
    padding: 10,
    flexDirection: "column",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingVertical: 15,
  },
  navigationContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderColor: "white",
    marginVertical: 4,
  },
  buttonLabel: {
    color: theme.primaryLight,
    fontSize: 18,
    fontWeight: "500",
  },
  balanceContainer: {
    padding: 16,
  },
  balance: {
    color: theme.primaryLight,
    textAlign: "center",
    fontSize: 40,
  },
  actionContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: 16,
    marginBottom: 8,
  },
  action: {
    borderWidth: 1,
    borderRadius: 5,
    width: 120,
    borderColor: "gray",
  },
  actionLabel: {
    color: "gray",
    fontSize: 16,
    fontWeight: "500",
  },
});

export default HomeNav;
