import { View, Text, StyleSheet, SafeAreaView, ScrollView } from "react-native";
import React from "react";
import { commonStyles, theme } from "../config/config";
import HomeNav from "../components/home/HomeNav";
import Brand from "../components/common/Brand";

const HomeScreen = ({ navigation }) => {
  return (
    <View style={commonStyles.screen}>
      <SafeAreaView style={{ flex: 1, borderColor: theme.primaryLight }}>
        <HomeNav navigation={navigation} />

        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: theme.primaryDark,
              margin: 10,
            }}
          >
            <Brand fontSize={32} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  text: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
  },
});

export default HomeScreen;
