import { View, Text, StyleSheet, SafeAreaView } from "react-native";
import React from "react";
import { commonStyles } from "../config/config";

const TemplateScreen = ({ navigation }) => {
  return (
    <View style={commonStyles.screen}>
      <SafeAreaView style={commonStyles.safeArea}>
        <View style={styles.container}>
          <Text style={styles.text}>Hello OnnPay!</Text>
        </View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  text: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
  },
});

export default TemplateScreen;
