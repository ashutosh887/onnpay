import { View, StyleSheet, SafeAreaView, Text, Image } from "react-native";
import React from "react";
import { commonStyles, description, theme } from "../config/config";
import IconButton from "../components/common/IconButton";

import logo from "../../assets/icon.png";
import metamask from "../../assets/metamask.png";
import google from "../../assets/google.png";
import consensys from "../../assets/consensys.png";
import linea from "../../assets/linea.png";
import infura from "../../assets/infura.png";
import Brand from "../components/common/Brand";

const LoginScreen = ({ navigation }) => {
  return (
    <View style={commonStyles.screen}>
      <SafeAreaView style={commonStyles.safeArea}>
        <View style={styles.container}>
          <View style={styles.logoContainer}>
            <Image source={logo} style={styles.logo} />
            <Brand fontSize={32} />
            <Text style={styles.description}>{description}</Text>
          </View>

          <View style={styles.buttonsContainer}>
            <IconButton
              text="Login with Metamask"
              logo={metamask}
              action={() => navigation.navigate("Home")}
            />
            <IconButton text="Login with Google" logo={google} />

            <IconButton
              text="Trouble getting started?"
              type="default"
              action={() => navigation.navigate("Support")}
            />
          </View>

          <View style={styles.poweredContainer}>
            <Text style={commonStyles.lightText}>Powered by</Text>

            <View style={styles.powered}>
              <Image source={infura} style={styles.poweredLogo} />
              <Image source={consensys} style={styles.poweredLogo} />
              <Image source={linea} style={styles.poweredLogo} />
            </View>
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center",
    padding: 40,
  },
  logoContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    width: 160,
    height: 160,
  },
  description: {
    fontSize: 12,
    color: theme.primaryLightGray,
    margin: 5,
  },
  buttonsContainer: {
    borderColor: theme.primaryMidGray,
    borderWidth: 1,
    padding: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
  },
  poweredContainer: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  powered: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 10,
  },
  poweredLogo: {
    height: 24,
    width: 24,
    marginHorizontal: 4,
  },
});

export default LoginScreen;
