import {
  View,
  Text,
  Linking,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Button,
} from "react-native";
import React from "react";
import { commonStyles, email, theme } from "../config/config";
import IconButton from "../components/common/IconButton";

const SupportScreen = ({navigation}) => {
  const handleEmailPress = () => {
    Linking.openURL(`mailto:${email}`);
  };

  return (
    <View style={commonStyles.screen}>
      <SafeAreaView style={commonStyles.safeArea}>
        <View style={styles.container}>
          <Text style={styles.text}>
            If you are unable to access your OnnPay Account, reach out to us at
          </Text>
          <TouchableOpacity onPress={handleEmailPress}>
            <Text style={styles.emailLink}>{email}</Text>
          </TouchableOpacity>

          <IconButton text="Back to Login" type="default" action={() => navigation.navigate('Login')} />
        </View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20
  },
  text: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center'
  },
  emailLink: {
    color: theme.primaryBlue,
    fontSize: 20,
    margin: 5,
    textDecorationLine: "underline",
  },
  backButton: {
    color: theme.primaryYellow,
    margin: 10
  }
});

export default SupportScreen;
