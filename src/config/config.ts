import { Platform, StatusBar } from "react-native";

export const email = 'devsapienshq@gmail.com'

export const balance = "$88.7"

export const username = "@ashutosh887"

export const theme = {
  primaryDark: "#0F1117",
  primaryLight: "#F7F7F8",
  primaryYellow: "#FFF36D",
  primaryDarkGray: "#777777",
  primaryMidGray: "#333333",
  primaryLightGray: "#EAEAEA",
  primaryBlue: '#19B5FE'
};

export const description = "Simplifying Web3 Payments"

export const commonStyles = {
  screen: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  safeArea: {
    flex: 1,
    backgroundColor: theme.primaryDark,
  },
  lightText: {
    color: theme.primaryLight
  },
  darkText: {
    color: theme.primaryDark
  },
  yellowText: {
    color: theme.primaryYellow
  }
};
