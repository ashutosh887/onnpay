<img src="./assets/icon.png" alt="onnpay" width="120"/>

# OnnPay:

Simplifying Web3 Payments with OnnPay!

- Goals: Shift / Inspire / Recognize

- OnnPay is a Web3 Payments application that can be used to resolve both on-chain and off-chain transactions.

- It allows users to scan QR codes and promotes massive adoption of Web3 currencies.

## Tech stack:
- Metamask React Native SDK: Authentication
- Linea: A developer-ready zkEVM rollup for scaling Ethereum dapps for multi-chain support
- Truffle: used for developing smart contracts
- Metamask Snaps: for detailed analysis of transactions
- React Native
- Google Firebase: For Web2 Authentication

## Preview:
<img src="./assets/preview.png" alt="preview" width="240"/>

## Bounties:
- To Infura and beyond
- Mobile Fun with Metamask SDK
- IYKYK Linea Edition
- Oh Snap!
- Make a DAPP that slaps, no cap!